DROP TABLE IF EXISTS dim_dates CASCADE;
CREATE TABLE dim_dates AS (
    SELECT DISTINCT
        to_date(connection_date::varchar, 'YYYYMMDD')   as date_nk
        , connection_date                               as date_int
        , to_char(to_date(connection_date::varchar, 'YYYYMMDD'), 'Day')             as weekday
        , date_trunc('week', to_date(connection_date::varchar, 'YYYYMMDD'))::date   as week_start_date
        , date_trunc('week',
            to_date(connection_date::varchar, 'YYYYMMDD')+ INTERVAL '7 days')::date  as week_end_date
        , date_trunc('week', to_date(connection_date::varchar, 'YYYYMMDD'))::date || ' - ' ||
          date_trunc('week', to_date(connection_date::varchar, 'YYYYMMDD')+ INTERVAL '7 days')::date as week_period
    FROM src_wifi_log
    ORDER BY 1
);

CREATE OR REPLACE VIEW dim_users AS
SELECT
    user_id
     , min(d.date_nk)                                             as first_connection_date
FROM anlt_wifi_log l
JOIN dim_dates d ON d.date_int = l.connection_date
GROUP BY 1;