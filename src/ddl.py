import psycopg2
from sqlalchemy import create_engine
from sqlalchemy.dialects import postgresql
import pandas as pd
import boto3
from misc import execute_queries

# Establish connection with postgresql docker container
user = 'postgres'
password = 'testpass'
connection_string = 'postgresql+psycopg2://%s:%s@localhost:5432/postgres' % (user, password)
engine = create_engine(connection_string)

# Load source data
aws = boto3.session.Session(profile_name='ozhyrenkov')
s3 = aws.client('s3')

response = s3.get_object(Bucket='gidno-public-tmp-bucket', Key="WIFI_connections_home_assignment.csv")
status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
if status == 200:
    print(f"Successful S3 get_object response. Status - {status}")
    data = pd.read_csv(response.get("Body"))

engine.execute('DROP TABLE IF EXISTS src_wifi_log CASCADE')
data.to_sql('src_wifi_log', 
            engine, 
            index = False, 
            if_exists = 'fail',
            dtype = {
                'connection_date': postgresql.INTEGER,
                'user_id': postgresql.BIGINT,
                'start_hour': postgresql.SMALLINT,
                'start_minute': postgresql.SMALLINT,
                'end_hour': postgresql.SMALLINT,
                'end_minute': postgresql.SMALLINT
            })

# execute ddl statements:
## Initial ddl with basic anlt table
execute_queries(engine, 'sql/00_ddl_initial.sql')

## Create dim tables
execute_queries(engine, 'sql/01_ddl_dims.sql')

## Create dact tables
execute_queries(engine, 'sql/02_ddl_facts.sql')

# Close the connection
engine.dispose()